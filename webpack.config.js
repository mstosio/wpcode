const path = require("path");
const webpack = require('webpack');

module.exports = {
    entry: {
        app: "./assets/js/script.js",
        libs: ['lodash']
    },
    output: {
        path: `${__dirname}/bundle/js`,
        filename: "[name].js" //[name] zmienai entry na nazwy wygenerowanych bundli
    },
    module: {
        rules: [
        {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
        }
    ]
    }
};