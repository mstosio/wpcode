/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/script.js":
/*!*****************************!*\
  !*** ./assets/js/script.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nif (window.matchMedia(\"(max-width: 800px)\").matches) {\n  slick();\n  slick2();\n} else {\n  slick3();\n}\n\nfunction slick() {\n  var _$$slick;\n\n  $('.description__info').slick({\n    asNavFor: '.description__box',\n    arrows: false\n  });\n  $('.description__box').slick((_$$slick = {\n    slidesToShow: 1,\n    slidesToScroll: 1,\n    asNavFor: '.description__info',\n    dots: false,\n    arrows: false,\n    centerMode: true\n  }, _defineProperty(_$$slick, \"centerMode\", true), _defineProperty(_$$slick, \"focusOnSelect\", true), _$$slick));\n}\n\nfunction slick2() {\n  $('.carousel').slick({\n    arrows: true,\n    slidesToShow: 2,\n    slidesToScroll: 1,\n    infinite: true,\n    speed: 300,\n    focusOnSelect: true // vertical: true,\n    // verticalSwiping: true\n\n  });\n}\n\nfunction slick3() {\n  $('.carousel').slick({\n    arrows: true,\n    slidesToShow: 3,\n    slidesToScroll: 1,\n    infinite: true,\n    speed: 300,\n    autoplay: true,\n    autoplaySpeed: 2000,\n    focusOnSelect: true,\n    vertical: true,\n    verticalSwiping: true\n  });\n}\n\nvar descItems = document.querySelectorAll(\".description__item\");\nvar descInfo = document.querySelector(\".description__info\");\nvar descData = document.querySelectorAll(\".description__data\");\nvar previous = \"\";\ndescItems.forEach(function (descItem) {\n  return descItem.addEventListener('click', function (event) {\n    event.stopPropagation();\n\n    if (previous != \"\") {\n      previous.classList.remove(\"active\");\n    }\n\n    previous = descItem;\n    descItem.classList.add(\"active\");\n    descData.forEach(function (item) {\n      if (event.target.closest(\".description__item\").dataset.name === item.dataset.info) {\n        descInfo.innerHTML = item.innerHTML;\n      }\n    });\n  });\n}); /////\n\njQuery(document).ready(function ($) {\n  //move nav element position according to window width\n  moveNavigation();\n  $(window).on('resize', function () {\n    !window.requestAnimationFrame ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);\n  }); //mobile version - open/close navigation\n\n  $('.cd-nav-trigger').on('click', function (event) {\n    event.preventDefault();\n    if ($('header').hasClass('nav-is-visible')) $('.moves-out').removeClass('moves-out');\n    $('header').toggleClass('nav-is-visible');\n    $('.cd-main-nav').toggleClass('nav-is-visible');\n    $('.cd-main-content').toggleClass('nav-is-visible');\n  }); //mobile version - go back to main navigation\n\n  $('.go-back').on('click', function (event) {\n    event.preventDefault();\n    $('.cd-main-nav').removeClass('moves-out');\n  }); //open sub-navigation\n\n  $('.cd-subnav-trigger').on('click', function (event) {\n    event.preventDefault();\n    $('.cd-main-nav').toggleClass('moves-out');\n  });\n\n  function moveNavigation() {\n    var navigation = $('.cd-main-nav-wrapper');\n    var screenSize = checkWindowWidth();\n\n    if (screenSize) {\n      //desktop screen - insert navigation inside header element\n      navigation.detach();\n      navigation.insertBefore('.cd-nav-trigger');\n    } else {\n      //mobile screen - insert navigation after .cd-main-content element\n      navigation.detach();\n      navigation.insertAfter('.cd-main-content');\n    }\n  }\n\n  function checkWindowWidth() {\n    var mq = window.getComputedStyle(document.querySelector('header'), '::before').getPropertyValue('content').replace(/\"/g, '').replace(/'/g, \"\");\n    return mq == 'mobile' ? false : true;\n  }\n});\n\n//# sourceURL=webpack:///./assets/js/script.js?");

/***/ })

/******/ });