if (window.matchMedia("(max-width: 800px)").matches) {
    slick();
    slick2();  
} else {
    slick3();
}


function slick(){
  $('.description__info').slick({
    asNavFor: '.description__box',
    arrows: false
  });

  $('.description__box').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.description__info',
    dots: false,
    arrows: false,
    centerMode: true,
    centerMode: true,
    focusOnSelect: true
  });     
}



function slick2(){
  $('.carousel').slick({
    arrows: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    infinite: true,
    speed: 300,
    focusOnSelect: true
    // vertical: true,
    // verticalSwiping: true
  });
}

function slick3(){
  $('.carousel').slick({
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 2000,
    focusOnSelect: true,
    vertical: true,
    verticalSwiping: true
  });
}

const descItems = document.querySelectorAll(".description__item");
const descInfo = document.querySelector(".description__info");
const descData = document.querySelectorAll(".description__data");
let previous = "";


descItems.forEach(descItem => descItem.addEventListener('click', function(event){
        event.stopPropagation();

        if(previous != ""){
                previous.classList.remove("active");
        }
 
        previous = descItem;

        descItem.classList.add("active");
       
            descData.forEach(item => {
                if(event.target.closest(".description__item").dataset.name === item.dataset.info){
                         descInfo.innerHTML = item.innerHTML;
                }
            }
        );
}))


/////
jQuery(document).ready(function($){
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});

	//mobile version - open/close navigation
	$('.cd-nav-trigger').on('click', function(event){
		event.preventDefault();
		if($('header').hasClass('nav-is-visible')) $('.moves-out').removeClass('moves-out');
		
		$('header').toggleClass('nav-is-visible');
		$('.cd-main-nav').toggleClass('nav-is-visible');
		$('.cd-main-content').toggleClass('nav-is-visible');
	});

	//mobile version - go back to main navigation
	$('.go-back').on('click', function(event){
		event.preventDefault();
		$('.cd-main-nav').removeClass('moves-out');
	});

	//open sub-navigation
	$('.cd-subnav-trigger').on('click', function(event){
		event.preventDefault();
		$('.cd-main-nav').toggleClass('moves-out');
  });
  
 

	function moveNavigation(){
		var navigation = $('.cd-main-nav-wrapper');
  		var screenSize = checkWindowWidth();
        if ( screenSize ) {
        	//desktop screen - insert navigation inside header element
			navigation.detach();
			navigation.insertBefore('.cd-nav-trigger');
		} else {
			//mobile screen - insert navigation after .cd-main-content element
			navigation.detach();
			navigation.insertAfter('.cd-main-content');
		}
	}

	function checkWindowWidth() {
		var mq = window.getComputedStyle(document.querySelector('header'), '::before').getPropertyValue('content').replace(/"/g, '').replace(/'/g, "");
		return ( mq == 'mobile' ) ? false : true;
	}
});